import telegram
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
import logging

# Enable logging
logging.basicConfig(
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        level=logging.INFO)

logger = logging.getLogger(__name__)


# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.
def start(bot, update):
    bot.sendMessage(update.message.chat_id, text='Hi!')


def help(bot, update):
    bot.sendMessage(update.message.chat_id, text='Help!')


def echo(bot, update):
    bot.sendMessage(update.message.chat_id, text=update.message.text)


def error(bot, update, error):
    logger.warn('Update "%s" caused error "%s"' % (update, error))

def get_photo(bot, update):
    to_create_photo = update.message.photo


def create(bot, update):
    bot.sendPhoto(chat_id=update.message.chat_id, photo=to_create_photo)


def echo_photo(bot, update):
    bot.sendMessage(chat_id=update.message.chat_id, text='new_photo')
    newphoto = bot.getFile(update.message.photo[-1].file_id)
    newphoto.download('photo{}'.format(update.message.photo[-1].file_id))
    print("OK")
    print(update.message.photo[-1])
    bot.sendPhoto(chat_id=update.message.chat_id, photo=update.message.photo[-1].file_id)


def echo_text(bot, update):
    bot.sendMessage(chat_id=update.message.chat_id, text='new_text')

def main():
    # Create the EventHandler and pass it your bot's token.
    updater = Updater("230080877:AAFXtDwberqj10aFtkupkb4GEeqK0zg7hsU")

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help))
    dp.addHandler(CommandHandler("create", create))
    dp.add_handler(MessageHandler([Filters.text], echo_text))
    dp.add_handler(MessageHandler([Filters.photo], echo_photo))
    # on noncommand i.e message - echo the message on Telegram
    dp.add_handler(MessageHandler([Filters.text], echo))


    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until the you presses Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()

if __name__ == '__main__':
    main()
